package com.polda.listmahasiswa;

public class Jurusan {
    private String namaJurusan;
    private String tahun;
    private int jumlah;

    public Jurusan() {}

    public Jurusan(String tahun, int jumlah) {
        this.tahun = tahun;
        this.jumlah = jumlah;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public String getNamaJurusan() {
        return namaJurusan;
    }

    public void setNamaJurusan(String namaJurusan) {
        this.namaJurusan = namaJurusan;
    }
}
