package com.polda.listmahasiswa;

import java.io.Serializable;

public class Mahasiswa implements Serializable {
    private String npm;
    private String nama;
    private String gender;
    private String foto;
    private String sekolah;
    private String noHp;
    private String alamat;

    public Mahasiswa() {}

    public Mahasiswa(String npm, String nama, String gender, String foto) {
        this.npm = npm;
        this.nama = nama;
        this.gender = gender;
        this.foto = foto;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getSekolah() {
        return sekolah;
    }

    public void setSekolah(String sekolah) {
        this.sekolah = sekolah;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
