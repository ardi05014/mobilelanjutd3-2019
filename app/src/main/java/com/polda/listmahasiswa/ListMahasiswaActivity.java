package com.polda.listmahasiswa;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ListMahasiswaActivity extends AppCompatActivity {

    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_mahasiswa);

        Intent intent = getIntent();
        String jurusan = intent.getStringExtra("jurusan");
        String tahun = intent.getStringExtra("tahun");
        String jumlah = intent.getStringExtra("jumlah");
        toolbar = getSupportActionBar();
        toolbar.setTitle(jurusan + " Angkatan " + tahun);
        toolbar.setSubtitle("Total: " + jumlah + " Mahasiswa");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


}
