package com.polda.listmahasiswa;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class JurusanAdapter extends RecyclerView.Adapter<JurusanAdapter.ViewHolder> {

    private Context context;
    private List<Jurusan> list;

    public JurusanAdapter(Context context, List<Jurusan> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_jurusan, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Jurusan obj = list.get(position);

        holder.tahun.setText(obj.getTahun());
        holder.jumlah.setText("Total: " + Integer.toString(obj.getJumlah()) + " Mahasiswa");

        holder.box_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ListMahasiswaActivity.class);
                intent.putExtra("jurusan", obj.getNamaJurusan());
                intent.putExtra("tahun", obj.getTahun());
                intent.putExtra("jumlah", Integer.toString(obj.getJumlah()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tahun;
        private TextView jumlah;
        private LinearLayout box_linear;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tahun = itemView.findViewById(R.id.tahun);
            jumlah = itemView.findViewById(R.id.jumlah);
            box_linear = itemView.findViewById(R.id.box_linear);
        }
    }
}
