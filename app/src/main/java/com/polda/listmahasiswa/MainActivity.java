package com.polda.listmahasiswa;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {

    private ActionBar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();

        loadFragment(new KimiaFragment());

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        toolbar.setTitle("Kimia");
    }

    private boolean loadFragment(Fragment fragment) {
        if(fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_kim:
                    toolbar.setTitle("Kimia");
                    fragment = new KimiaFragment();
                    break;
                case R.id.navigation_bio:
                    toolbar.setTitle("Biologi");
                    fragment = new BiologiFragment();
                    break;
                case R.id.navigation_mat:
                    toolbar.setTitle("Matematika");
                    fragment = new MatematikaFragment();
                    break;
                case R.id.navigation_fis:
                    toolbar.setTitle("Fisika");
                    break;
                case R.id.navigation_kom:
                    toolbar.setTitle("Ilmu Komputer");
                    break;
            }
            return loadFragment(fragment);
        }
    };
}
